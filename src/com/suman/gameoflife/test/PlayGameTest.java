package com.suman.gameoflife.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.suman.gameoflife.PlayGame;

@RunWith(PowerMockRunner.class)
public class PlayGameTest {

	private PlayGame playGame = new PlayGame(4, 4);

	boolean[][] boardBefore;
	boolean[][] boardAfter;

	@Before
	public void init() {
		// Draw a 4X4 board
		

		/*
		 * O O O . 
		 * . O O .
		 * . O . O
		 * O . . .
		 */
		boardBefore = new boolean[][] { { true, true, true, false }, { false, true, true, false }, { false, true, false, true },
				{ true, false, false, false } };

		/*
		 * O . O . 
		 * . . . O
		 * O O . .
		 * . . . .
		 */
		boardAfter = new boolean[][] { { true, false, true, false }, { false, false, false, true }, { true, true, false, false },
				{ false, false, false, false } };

	}
	
	@Test
	public void testGetNeighborCount() throws Exception{
	
		Whitebox.setInternalState(playGame, "board", boardBefore);
		
		int livingCellCount = WhiteboxImpl.invokeMethod(playGame, "getNeighborCount", 1, 1);
		Assert.assertEquals(5, livingCellCount);
		
		livingCellCount = WhiteboxImpl.invokeMethod(playGame, "getNeighborCount", 0, 0);
		Assert.assertEquals(2, livingCellCount);
		
		livingCellCount = WhiteboxImpl.invokeMethod(playGame, "getNeighborCount", 0, 3);
		Assert.assertEquals(2, livingCellCount);
		
		livingCellCount = WhiteboxImpl.invokeMethod(playGame, "getNeighborCount", 3, 2);
		Assert.assertEquals(2, livingCellCount);
		
		livingCellCount = WhiteboxImpl.invokeMethod(playGame, "getNeighborCount", 3, 3);
		Assert.assertEquals(1, livingCellCount);
	}

	@Test
	public void testPlayGame() {

		Whitebox.setInternalState(playGame, "board", boardBefore);
		playGame.playGame();
		boolean[][] boardAfterPlay = (boolean[][]) Whitebox.getInternalState(playGame, "board");

		// verify the state changed to expected.
		for (int i = 0; i < boardBefore.length; i++) {
			for (int j = 0; j < boardBefore[0].length; j++) {
				Assert.assertEquals(boardAfter[i][j], boardAfterPlay[i][j]);
			}
		}

	}
	
}
