package com.suman.gameoflife;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PlayGame {
	private boolean[][] board;
	private Random randomGenerator;
	private int row;
	private int column;

	/**
	 * Initializes the game board with initial parameters
	 * @param x number of rows in board
	 * @param y
	 */
	public PlayGame(int x, int y) {
		row = x;
		column = y;
		board = new boolean[row][column];
		randomGenerator = new Random();
		populateBoard();

	}

	/**
	 * Play the game
	 */
	public void playGame() {

		List<Integer> dyingCells = new ArrayList<Integer>();
		List<Integer> wakingCells = new ArrayList<Integer>();

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				int count = getNeighborCount(i, j);
				if (board[i][j]) {
					if (count < 2 || count > 3) {
						dyingCells.add(i);
						dyingCells.add(j);
					}
				} else {
					if (count == 3) {
						wakingCells.add(i);
						wakingCells.add(j);
					}
				}
			}
		}

		changeCellsState(dyingCells, false);
		changeCellsState(wakingCells, true);

	}

	/**
	 * Prints the game board on console
	 */
	public void printBoard() {
		for (boolean[] row : board) {
			for (boolean value : row) {
				if (value) {
					System.out.print(" O");
				} else {
					System.out.print(" .");
				}
			}
			System.out.println("");
		}
	}

	/**
	 * Returns the alive cells around the given cordinates
	 * 
	 * @param x
	 *            X cordinate of current cell
	 * @param y
	 *            Y cordinate of current cell
	 * @return total count of alive cells
	 */
	private int getNeighborCount(int x, int y) {
		int aliveCount = 0;
		int minRow = x == 0 ? 0 : x - 1;
		int minCol = y == 0 ? 0 : y - 1;
		int maxRow = x == row - 1 ? row - 1 : x + 1;
		int maxCol = y == column - 1 ? column - 1 : y + 1;

		for (int i = minRow; i <= maxRow; i++) {
			for (int j = minCol; j <= maxCol; j++) {
				if (i == x && j == y) {
					continue;
				}
				if (board[i][j]) {
					aliveCount++;
				}
			}
		}
		return aliveCount;
	}

	/**
	 * Populates board for first time.
	 */
	private void populateBoard() {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				board[i][j] = randomGenerator.nextBoolean();
			}
		}

	}

	private void changeCellsState(List<Integer> cordinates, boolean state) {
		for (int i = 0; i < cordinates.size(); i++) {
			board[cordinates.get(i)][cordinates.get(++i)] = state;
		}
	}

}
