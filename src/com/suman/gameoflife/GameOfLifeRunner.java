package com.suman.gameoflife;

import java.util.Scanner;

public class GameOfLifeRunner {

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			System.out.println("+++++++++++ Welcome to the Conway's Game of Life ++++++++++++++");
			System.out.println("\nPlease enter the dimension of the game board: \nRows:");

			int row = 0;
			int col = 0;
			
			boolean notFound = true;
			while (notFound) {
				if (in.hasNextInt()) {
					row = in.nextInt();
					if (row > 0) {
						notFound = false;
					}
				}
				if (notFound) {
					System.out.println("Please Provide a valid positive number for row.");
					in.nextLine();
				}
			}

			System.out.println("Columns: ");
			notFound = true;
			while (notFound) {
				if (in.hasNextInt()) {
					col = in.nextInt();
					if (col > 0) {
						notFound = false;
					}
				}
				if (notFound) {
					System.out.println("Please Provide a valid positive number for column.");
					in.nextLine();
				}
			}

			// Initialize the board
			PlayGame game = new PlayGame(row, col);

			System.out.println("Your game board is populated successfully");
			game.printBoard();

			while (true) {
				System.out.println("\nDo you want to play the game? (Y/N)");
				String answer = in.next();
				if ("N".equalsIgnoreCase(answer)) {
					System.out.println("Goodbye!");
					return;
				} else if ("Y".equalsIgnoreCase(answer)) {
					game.playGame();
					System.out.println("\nNew state of the game:");
					game.printBoard();
				} else {
					System.out.println("Please Enter Y to play game or N to exit.");
				}
			}
		} finally {
			// Do Nothing
		}
	}

}
